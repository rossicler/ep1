#ifndef CAMPO_HPP
#define CAMPO_HPP

class Campo
{
protected:
	char matrizPrimaria[50][50];
	char matrizSecundaria[50][50];
	int contadorMatriz[50][50];	
	int velocidade;
	int segundos;
	int posx, posy;
	char caracterPreenchido;
	int tamanho;
public:
	Campo();
	~Campo();

	int getTamanho();
	void setTamanho(int tamanho);
	void zerarContadorMatriz(Campo * campo);
	void setContadorMatriz(int posx, int posy);
	int getContadorMatriz(int posx, int posy);
	void setMatrizPrimaria(int posx, int posy, char caracterPreenchido);
	char getMatrizPrimaria(int posx, int posy);
	void setMatrizSecundaria(int posx, int posy, char caracterPreenchido);
	char getMatrizSecundaria(int posx, int posy);
	void mostrarMatriz(Campo * campo);
	int getVelocidade();
	void setVelocidade(int velocidade);
	void setSegundos(int segundos, int velocidade);
	int getSegundos();
	void regrasVida(Campo * campo);

	
};


#endif