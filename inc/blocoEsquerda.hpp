#ifndef BLOCO_ESQUERDA_HPP
#define BLOCO_ESQUERDA_HPP

#include "campo.hpp"

class BlocoEsquerda : public Campo{
public:
	BlocoEsquerda();
	~BlocoEsquerda();

	void setBloco(Campo * blocoEsquerda);
	
};


#endif