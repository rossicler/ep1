#ifndef BLOCO_DIREITA_HPP
#define BLOCO_DIREITA_HPP
#include "campo.hpp"

class BlocoDireita : public Campo{
public:
	BlocoDireita();
	~BlocoDireita();

	void setBloco(Campo * blocoDireita);

};

#endif