#include <iostream>
#include "campo.hpp"

using namespace std;

Campo::Campo(){
	for(int i=0;i<50;i++){
		for(int k=0;k<50; k++){
			matrizPrimaria[i][k] = '-';
			matrizSecundaria[i][k] = '-';
		}
	}
	for(int i=0;i<50;i++){
		for(int k=0;k<50; k++){
			contadorMatriz[i][k] = 0;
		}
	}
	velocidade = 0;
	segundos = 0;
	posx = 0;
	posy = 0;
	caracterPreenchido = ' ';
	velocidade = 0;
	segundos = 0;
	tamanho = 50;
};
Campo::~Campo(){};

int Campo::getTamanho(){
	return tamanho;
}
void Campo::setTamanho(int tamanho){
	this->tamanho = tamanho;
}
void Campo::zerarContadorMatriz(Campo * campo){
	for(int i=0;i<campo->getTamanho();i++){
		for(int k=0;k<campo->getTamanho(); k++){
			contadorMatriz[i][k] = 0;
		}
	}
}
void Campo::setContadorMatriz(int posx, int posy){
	contadorMatriz[posx][posy] ++;
}
int Campo::getContadorMatriz(int posx, int posy){
	return contadorMatriz[posx][posy];
}
void Campo::setMatrizPrimaria(int posx, int posy, char caracterPreenchido){
		matrizPrimaria[posx][posy] = caracterPreenchido;
}
char Campo::getMatrizPrimaria(int posx, int posy){
	return matrizPrimaria[posx][posy];
}
void Campo::setMatrizSecundaria(int posx, int posy, char caracterPreenchido){
		matrizSecundaria[posx][posy] = caracterPreenchido;
}
char Campo::getMatrizSecundaria(int posx, int posy){
	return matrizSecundaria[posx][posy];
}
void Campo::mostrarMatriz(Campo * campo){
	for(int i=0;i<campo->getTamanho()-23;i++){
		for(int k=0;k<campo->getTamanho()-13;k++){
			cout << campo->getMatrizPrimaria(i,k);
		}
		cout << endl;
	}
}

int Campo::getVelocidade(){
	return velocidade;
}
void Campo::setVelocidade(int velocidade){
	if(velocidade == 1){
		this->velocidade = 1000000;
	}
	if(velocidade == 2){
		this->velocidade = 500000;
	}
	if(velocidade == 3){
		this->velocidade = 100000;
	}
	if(velocidade == 4){
		this->velocidade = 50000;
	}
	if(velocidade == 5){
		this->velocidade = 10000;
	}
	if(velocidade < 1 and velocidade > 5){
		cout << "Opção incorreta, selecione uma opção válida!!" << endl;
	}
}

void Campo::setSegundos(int segundos,int velocidade){
	if(velocidade == 1000000){
		this->segundos = segundos;
	}
	if(velocidade == 500000){
		this->segundos = segundos * 2;
	}
	if(velocidade == 100000){
		this->segundos = segundos * 10;
	}
	if(velocidade == 50000){
		this->segundos = segundos * 20;
	}
	if(velocidade == 10000){
		this->segundos = segundos * 100;
	}
}
int Campo::getSegundos(){
	return segundos;
}

void Campo::regrasVida(Campo * campo){
	int i,k;
	for(i=1;i<=campo->getTamanho()-11;i++){
			for(k=1;k<=campo->getTamanho()-1;k++){
				if(campo->getMatrizPrimaria(i,k) == '*'){
					if(campo->getMatrizPrimaria(i-1,k-1) == '*'){
						campo->setContadorMatriz(i,k);
					}
					if(campo->getMatrizPrimaria(i-1,k) == '*'){
						campo->setContadorMatriz(i,k);
					}
					if(campo->getMatrizPrimaria(i-1,k+1) == '*'){
						campo->setContadorMatriz(i,k);
					}
					if(campo->getMatrizPrimaria(i,k-1) == '*'){
						campo->setContadorMatriz(i,k);
					}
					if(campo->getMatrizPrimaria(i,k+1) == '*'){
						campo->setContadorMatriz(i,k);
					}
					if(campo->getMatrizPrimaria(i+1,k-1) == '*'){
						campo->setContadorMatriz(i,k);
					}
					if(campo->getMatrizPrimaria(i+1,k) == '*'){
						campo->setContadorMatriz(i,k);
					}
					if(campo->getMatrizPrimaria(i+1,k+1) == '*'){
						campo->setContadorMatriz(i,k);
					}
					if(campo->getContadorMatriz(i,k) < 2 or campo->getContadorMatriz(i,k) > 3){
						campo->setMatrizSecundaria(i,k,'-');
					}
					else{
						campo->setMatrizSecundaria(i,k,'*');
					}
				}
				if(campo->getMatrizPrimaria(i,k) == '-'){
					if(campo->getMatrizPrimaria(i-1,k-1) == '*'){
						campo->setContadorMatriz(i,k);
					}
					if(campo->getMatrizPrimaria(i-1,k) == '*'){
						campo->setContadorMatriz(i,k);
					}
					if(campo->getMatrizPrimaria(i-1,k+1) == '*'){
						campo->setContadorMatriz(i,k);
					}
					if(campo->getMatrizPrimaria(i,k-1) == '*'){
						campo->setContadorMatriz(i,k);
					}
					if(campo->getMatrizPrimaria(i,k+1) == '*'){
						campo->setContadorMatriz(i,k);
					}
					if(campo->getMatrizPrimaria(i+1,k-1) == '*'){
						campo->setContadorMatriz(i,k);
					}
					if(campo->getMatrizPrimaria(i+1,k) == '*'){
						campo->setContadorMatriz(i,k);
					}
					if(campo->getMatrizPrimaria(i+1,k+1) == '*'){
						campo->setContadorMatriz(i,k);
					}
					if(campo->getContadorMatriz(i,k) == 3){
						campo->setMatrizSecundaria(i,k,'*');
					}
				}
			}
	}
	for(i=0;i<=campo->getTamanho();i++){
		campo->setMatrizSecundaria(0,i,'-');
		campo->setMatrizSecundaria(i,0,'-');
		campo->setMatrizSecundaria(campo->getTamanho()-10,i,'-');
		campo->setMatrizSecundaria(i,campo->getTamanho(),'-');
	}
	for(i=0;i<=campo->getTamanho()-10;i++){
			for(k=0;k<campo->getTamanho();k++){
				campo->setMatrizPrimaria(i,k,campo->getMatrizSecundaria(i,k));
			}
	}
	campo->zerarContadorMatriz(campo);
}

