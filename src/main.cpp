#include <iostream>
#include <unistd.h>
#include "campo.hpp"
#include "blocoEsquerda.hpp"
#include "blocoDireita.hpp"
#include "gun.hpp"

using namespace std;

int main(int argc, char const *argv[])
{
	BlocoEsquerda * blocoEsquerda = new BlocoEsquerda();
	BlocoDireita * blocoDireita = new BlocoDireita();
	Gun * gun = new Gun();
	Campo * campo = new Campo();

	int velocidade = 0;
	int segundos;
	int geracoes = 0;

	blocoEsquerda->setBloco(campo);
	blocoDireita->setBloco(campo);
	gun->setGun(campo);

	cout << "\e[H\e[2J"; //Limpa a tela do terminal do linux

	cout << endl << endl;
	cout << "    ******   ****   **   **  *****     *****  *****     *      *  *****  *****     " << endl;
	cout << "    *       *    *  * * * *  *         *   *  *         *      *  *      *	        " << endl;
	cout << "    *   **  * ** *  *  *  *  ***       *   *  ***       *      *  ***    *** 		" << endl;
	cout << "    *    *  *    *  *     *  *         *   *  *         *      *  *      *	        " << endl;
	cout << "    ******  *    *  *     *  *****     *****  *         *****  *  *      *****     " << endl;
	cout << endl << endl;

	cout << "    Escolha uma velocidade de 1 á 5: ";
	cin >> velocidade;
	campo->setVelocidade(velocidade);
	cout << "\e[H\e[2J";
	cout << endl << endl;
	cout << "    ******   ****   **   **  *****     *****  *****     *      *  *****  *****     " << endl;
	cout << "    *       *    *  * * * *  *         *   *  *         *      *  *      *	        " << endl;
	cout << "    *   **  * ** *  *  *  *  ***       *   *  ***       *      *  ***    *** 		" << endl;
	cout << "    *    *  *    *  *     *  *         *   *  *         *      *  *      *	        " << endl;
	cout << "    ******  *    *  *     *  *****     *****  *         *****  *  *      *****     " << endl;
	cout << endl << endl;
	cout << "    Escolha a quantidade de segundos de duração do programa: ";
	cin >> segundos;
	campo->setSegundos(segundos, campo->getVelocidade());
	cout << "\e[H\e[2J";
	campo->mostrarMatriz(campo);
	usleep(campo->getVelocidade());
	cout << "\e[H\e[2J";
	do{
		campo->regrasVida(campo);
		campo->mostrarMatriz(campo);
		geracoes++;
		cout << endl << endl << endl << endl;
		usleep(campo->getVelocidade());
		cout << "\e[H\e[2J";
	}while(geracoes<=campo->getSegundos()); //segundosObj.getSegundos()


	return 0;
}